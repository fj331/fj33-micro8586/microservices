package me.fwfurtado.books.infra;

import static org.assertj.core.api.Assertions.assertThat;

import me.fwfurtado.books.infra.AuthorClient.AuthorView;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.StubTrigger;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties.StubsMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner(stubsPerConsumer = true, consumerName = "book", ids = "me.fwfurtado:authors-api:+:stubs:6565", stubsMode = StubsMode.LOCAL)
public class AuthorContractTest {

    private RestTemplate restTemplate = new RestTemplate();

    @Test
    public void test() {
        AuthorView author = restTemplate.getForObject("http://localhost:6565/1", AuthorView.class);

        assertThat(author)
            .hasFieldOrPropertyWithValue("id", 1L)
            .hasFieldOrPropertyWithValue("name", "Alberto");
    }


}

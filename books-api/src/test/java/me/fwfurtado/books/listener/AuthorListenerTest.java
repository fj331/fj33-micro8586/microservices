package me.fwfurtado.books.listener;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.then;

import me.fwfurtado.books.listener.AuthorListener.CreatedAuthorEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.stubrunner.StubTrigger;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties.StubsMode;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner(stubsPerConsumer = true, consumerName = "book", ids = "me.fwfurtado:authors-api:+:stubs", stubsMode = StubsMode.LOCAL)
@AutoConfigureMessageVerifier
public class AuthorListenerTest {

    @MockBean
    private AuthorListener listener;

    @Captor
    private ArgumentCaptor<CreatedAuthorEvent> eventArgumentCaptor;


    @Autowired
    private StubTrigger trigger;

    @Test
    public void test() {

        trigger.trigger("fire");

        then(listener).should().handle(eventArgumentCaptor.capture());

        CreatedAuthorEvent event = eventArgumentCaptor.getValue();

        assertThat(event)
            .hasFieldOrPropertyWithValue("id", 1L)
            .hasFieldOrPropertyWithValue("name", "Thiago");

    }

}

package me.fwfurtado.books.refresh;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class RefreshController {

    private final String caelumURL;

    public RefreshController(@Value("${caelum.url}") String caelumURL) {
        this.caelumURL = caelumURL;
    }

    @GetMapping("caelum")
    Map<String, String> caelum() {
        HashMap<String, String> json = new HashMap<>();

        json.put("CAELUM_URL", caelumURL);

        return json;
    }
}

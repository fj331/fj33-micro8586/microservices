package me.fwfurtado.books.listener;

import lombok.Data;
import me.fwfurtado.books.configuration.AuthorBind;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
class AuthorListener {

    @StreamListener(value = AuthorBind.AUTHOR)
    void handle(CreatedAuthorEvent event) {
        System.out.println(event);
    }

    @Data
    static class CreatedAuthorEvent {

        private Long id;
        private String name;
    }
}

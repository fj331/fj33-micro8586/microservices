package me.fwfurtado.books.configuration;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

public interface AuthorBind {
    String AUTHOR = "authors";


    @Input(AUTHOR)
    MessageChannel channel();

}

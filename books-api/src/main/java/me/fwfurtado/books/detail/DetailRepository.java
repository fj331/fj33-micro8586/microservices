package me.fwfurtado.books.detail;

import java.util.Optional;
import me.fwfurtado.books.Book;

public interface DetailRepository {
    Optional<Book> findById(Long id);
}

package me.fwfurtado.infra;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import me.fwfurtado.login.LoginRepository;
import me.fwfurtado.shared.User;
import me.fwfurtado.shared.UserStatus;
import org.springframework.stereotype.Repository;

@Repository
class UserRepository implements LoginRepository {

    private static final String DEFAULT_PASSWORD = "{bcrypt}$2a$10$RHSFMti9WersV2p7KK/2/eU9jgHXSq38QHHiB/SvlFJX1JnnDrtK.";
    private static final Map<String, User> DATABASE = new HashMap<>();

    static {
        DATABASE.computeIfAbsent("fwfurtado", username -> new User(username, DEFAULT_PASSWORD, UserStatus.ACTIVE, new String[]{ "ROLE_USER"}));
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(DATABASE.get(username));
    }
}

package me.fwfurtado.login;

import java.util.Optional;
import me.fwfurtado.shared.User;

public interface LoginRepository {

    Optional<User> findByUsername(String username);
}

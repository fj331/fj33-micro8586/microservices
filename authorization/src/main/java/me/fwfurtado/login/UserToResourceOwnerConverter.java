package me.fwfurtado.login;

import java.util.List;
import me.fwfurtado.shared.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

@Component
class UserToResourceOwnerConverter {

    ResourceOwner convert(User user) {

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(user.getRoles());

        return new ResourceOwner(user.getUsername(), user.getPassword(), user.getStatus(), authorities);
    }
}

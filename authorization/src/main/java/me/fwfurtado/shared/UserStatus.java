package me.fwfurtado.shared;

public enum UserStatus {
    ACTIVE, INACTIVE;

    public boolean canDoLogin() {
        return this == ACTIVE;
    }
}

package me.fwfurtado.authors.configuration;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface AuthorBind {
    String AUTHOR = "authors";

    @Output(AUTHOR)
    MessageChannel channel();

}

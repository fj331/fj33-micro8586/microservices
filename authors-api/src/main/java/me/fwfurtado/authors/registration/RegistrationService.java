package me.fwfurtado.authors.registration;

import me.fwfurtado.authors.Author;
import me.fwfurtado.authors.configuration.AuthorBind;
import me.fwfurtado.authors.registration.RegistrationController.AuthorForm;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
class RegistrationService {

    private final RegistrationRepository repository;
    private final MessageChannel channel;

    RegistrationService(RegistrationRepository repository, @Output(AuthorBind.AUTHOR) MessageChannel channel) {
        this.repository = repository;
        this.channel = channel;
    }

    Long registerBy(AuthorForm form) {
        String name = form.getName();

        repository.findByName(name).ifPresent(author -> { throw new IllegalArgumentException("Authors already exist."); });

        Author author = new Author(name);

        repository.save(author);

        Message<Author> event = MessageBuilder.withPayload(author).build();

        channel.send(event);

        return author.getId();
    }
}

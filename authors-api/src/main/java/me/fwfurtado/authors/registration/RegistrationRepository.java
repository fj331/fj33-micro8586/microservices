package me.fwfurtado.authors.registration;

import java.util.Optional;
import me.fwfurtado.authors.Author;

public interface RegistrationRepository {

    Optional<Author> findByName(String name);

    void save(Author author);
}

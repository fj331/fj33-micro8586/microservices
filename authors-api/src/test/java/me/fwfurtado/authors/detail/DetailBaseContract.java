package me.fwfurtado.authors.detail;

import static org.mockito.Mockito.when;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import java.util.Optional;
import me.fwfurtado.authors.Author;
import me.fwfurtado.authors.configuration.AuthorBind;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.verifier.messaging.MessageVerifier;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMessageVerifier
public abstract class DetailBaseContract {

    @Autowired
    @Output(AuthorBind.AUTHOR)
    private MessageChannel channel;

    @Mock
    private DetailRepository repository;

    @Before
    public void setup() {
        Author alberto = new Author(1L, "Alberto");

        when(repository.findById(1L)).thenReturn(Optional.of(alberto));

        RestAssuredMockMvc.standaloneSetup(new DetailController(repository));
    }

    public void fireEvent() {

        Author thiago = new Author(1L, "Thiago");

        Message<Author> event = MessageBuilder.withPayload(thiago).build();

        channel.send(event);
    }
}

package contracts.book

import org.springframework.cloud.contract.spec.Contract

Contract.make {

    request {
        method GET()

        url '/1'

        headers {
            accept applicationJson()
        }
    }

    response {
        status OK()

        headers {
            contentType applicationJson()
        }

        body([
                "id": 1,
                "name": "Alberto"
        ])
    }

}
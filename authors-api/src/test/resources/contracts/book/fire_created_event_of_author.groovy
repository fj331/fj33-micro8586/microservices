package contracts.book

import org.springframework.cloud.contract.spec.Contract

Contract.make {

    label 'fire'

    input {
        triggeredBy('fireEvent()')
    }

    outputMessage {
        sentTo('authors')

        body([
                "id": 1,
                "name": "Thiago"
        ])
    }
}